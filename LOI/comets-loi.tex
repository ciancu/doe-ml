
\documentclass[11pt,onecolumn]{IEEEtran}
\usepackage{color,verbatim,url}
\newcommand{\red}[1]{ \textcolor{red}{#1}}
%\setlength\textheight{9.25in}
%\setlength\textwidth{6.5in} 
\newcommand\pialloc[1]{#1}
\newcommand\pihline{}
\long\def\comment#1{}
\long\def\note#1{{\em #1 }}
\def\parah#1{\vspace*{0.08in} \noindent{\bf #1:}}
\usepackage{soul}
\usepackage{marginnote}
\title{\bf Co-Designing the Machine Learning Ecosystem for
  Scientific Computing - COMETS \\ 
\normalsize
In Response to Funding Opportunity Announcement Number: DE-FOA-0001575
}

\author{
\small
{\bf Principal Investigator:}
Costin Iancu, Staff Scientist\\
Lawrence Berkeley National Laboratory\\
Email: {\tt cciancu@lbl.gov}; Phone: 510-495-2122\\~\\
\footnotesize
%{\bf Investigators:}\\
\begin{tabular}{l  l  r}\pihline
Costin Iancu & Lawrence Berkeley National Laboratory  \pialloc{&
  \$450,000, \$750,000, \$750,000}\\\pihline
Khaled Z. Ibrahim & Lawrence Berkeley National Laboratory
                    \pialloc{&}\\\pihline
Andrew Canning & Lawrence Berkeley National Laboratory
                 \pialloc{&}\\\pihline
Wibe de Jong & Lawrence Berkeley National Laboratory  \pialloc{&}\\\pihline
Sally Ellingson  & University of Kentucky \pialloc{&
  \$100,000, \$100,000, \$100,000}\\\pihline
Tatiana Shpeisman  & Intel \pialloc{&  \$100,000, \$100,000, \$100,000}\\\pihline
Rajkishore Barik  & Intel \pialloc{& }\\\pihline
{\bf Total Annual Budget: } & \pialloc{&  {\$650,000, \$950,000, \$950,000}}\\\pihline
\end{tabular}\\~\\

}


\date{}

\begin{document}



\maketitle
%\newpage


\vspace{-.5in}
Enabled by  recent increases in computational power, machine
learning (ML)  techniques have the potential to transform the process of
scientific discovery.  On the other hand, these techniques are
currently pushing  the boundaries of our software technology and
our algorithmic ability. To
draw an analogy with scientific computing, we are in the era before
MPI 1.0 was adopted. There are several problems that need to be
addressed:  1)  application domain experts need to collaborate with
data analysts using a common generic infrastructure; 2) existing infrastructures do not scale on
HPC systems, or with the models; 
and 3) customization for HPC systems cannot be  disruptive
to any existing and widely used data analysis infrastructures.

For ML to become mainstream, it is required  a concerted effort that addresses approaches
at both ends of the spectrum, at the systems software and programming
models as well as the application domain.  At one end,
success requires {\it productivity}, {\it interoperability}, {\it availability} and
{\it performance}. At  application domain, we need to demonstrate the
{\it transformative approaches to scientific discovery} and its
processes.   Our team comprises experts in systems software and programming models,
machine learning experts and application domain experts.   This
combination  allows us to attempt a co-design approach where we make
coordinated modifications, resulting in scientific
results together with a common framework demonstrated across
multiple domains. 


At the programming level, {\it productivity} needs to emphasize the ability
to prototype in High Level Languages, compose solvers and existing
methods in order to enable  fast exploration of the design space and
algorithmic approaches.  At this juncture and until we train the
next generation of dual experts in science and data analytics, productivity is perhaps the
most important attribute, in our view certainly 
more important than highest performance.  Our plan is to use a
combination of two existing infrastructures. Apache Spark gives us
a generic platform  for data analytics, together with high level
libraries for machine learning (MLLib) programmable in Scala, Python
and R. 
Latte from Intel gives us a generic platform for programming Deep Neural Networks (DNN). Latte provides a natural abstraction for specifying new layers in DNN via a domain-specific language. It hides low-level details including parallelization/vectorization, heterogeneous code generation, and domain-specific optimizations from the end-user. Latte also includes a communication runtime based on MPI for distributed memory data parallelism of deep neural network training. These
provide a common substrate for  data and domain experts.

{\it Interoperability} complements the productivity requirements and
facilitates the interaction between data and domain experts.  It
provides  the ability to call specialized MPI solvers from  data 
frameworks (Spark and Latte), as well as the ability to call into the
data framework from the scientific applications. Currently there
exists little experience in this area and we plan to perform research
to enable 
both alternatives.

There are two components to {\it performance}.  First, the
 infrastructures themselves need to provide satisfactory performance
 and  scalability.  Some of these are expected to come from 
an orchestrated evolution  of the ecosystem (e.g. Spark and Latte) in
the commercial domain. However, as our recent experiences
 indicate, there exists a dire necessity for HPC customization. The second
 component is the ability to tackle Big Models. The state of the art in ML tackles well
 Big Data, where a model fits and runs within one device but the volume of
 training  data is large. Big Models solve problems with a large
 number of variables and the model itself is parallelized across
 multiple devices. The state of the art approaches can scale a model
 up to few  (maybe tens) nodes, in particular for DNN approaches. We
 plan to address customization as needed and concentrate on improving
 the scalability of Big Models.    
{\it Availability} denotes the ability to run
on supercomputers, as well as the  choice of  the right method and
approach. As many ML methods have efficient implementations only on
GPUs, we plan to experiment and tune  Spark and Latte  on the upcoming
Intel KNL architecture, soon to be deployed in production at
NERSC. 

We plan to demonstrate transformative approaches in three
application domains: materials science, cancer treatment research and mass
spectrometry. These were selected to provide good coverage
of the systems software and programming models design space and we
will explore supervised and un-supervised ML and DNN methods.

One of the main objectives in materials science research is predicting
 high level materials properties related to targeted
applications to develop new materials that have better ``performance''
or  cost.  Recent work, such as supported by the
Materials Genome Initiative, has produced databases of parameters for
 materials and elements that can be used as a starting point and  very
expensive first-principles calculations are performed currently to predict new
material  properties.
Recently we have shown that ML can be used to predict with high
accuracy the different types of point defects found in intermetallic
compounds using decision trees replacing the costly first-principles
calculations. The knowledge of the type and concentration of defects
is very important for the structural properties of new materials as
well as conductivity in semi-conductors and optical properties of
insulating materials. We plan to apply other machine learning
techniques such as DNNs, as well as scale up the
problems to a much larger number of descriptors for predicting a wider
range of materials properties with higher accuracy.

The  prediction of adverse drug reactions would
revolutionize the pharmaceutical industry allowing for safer and
cheaper drugs on the market. This  will  increase the quality
of life for cancer patients that now rely on harsh drugs with many
adverse reactions.  Computationally expensive calculations are
performed to test interactions between drug candidates and a small
panel of known proteins that would result in unfavorable effects. It
is not feasible yet to have a full understanding of all
the potential off-target effects, i.e. drugs interacting with proteins in
which they are not intended to.  Molecular docking is an efficient
calculation that predicts interactions between drugs and proteins and
would allow for an exploration of a much larger library of
proteins. This method works well to predict protein-drug conformations
and rank active compounds over inactive compounds, but lacks an
accurate correlation with experimental binding affinities. Machine
learning techniques will be developed to take into account docking
scores and conformation structures to make better predictions. We also plan to explore the
usage of ML techniques to steer large scale molecular dynamic
simulations using NAMD, which is the first and very computationally
intensive step in this workflow.

Due to it sensitivity mass spectrometry can detect molecular species
at high sensitivity from complex mixtures, and mass spectra are often
the first data available on unknown samples in microbial ecology,
metabolomics, as well as analysis of metabolism for synthetic
biology. Natural samples can contain on the order of 100,000 molecular
species. The current identification accuracy hovers around 50-70\%,
while an accuracy of 99\% is needed. To identify not only the mass but
structural features of the species, a combinatorial approach of
pattern matching can be used that relies on matching against a subset
of standards (in the order of 1000), and by reconstructing molecular
species from knowledge on the way they fragment and transform
described in large fragmentation trees. Machine learning techniques
are ideally suited to accelerate the identification in mass
spectrometry. However, the number of variables that need to be
considered will be large, starting with thousands of spectra each with
multiple identifiers, to tens and hundreds of thousands of molecular
species and fragmentation finger prints. This particular problem will
allow us to validate our techniques to improve the scalability of Big Models.






We propose a collaboration of four institutions with a wide range
  of expertise. LBNL and the Intel Programming System Lab have an
  impressive track record in the area of systems software and
  programming models research. One of the notable accomplishments of 
the LBNL team is demonstrating in April 2016 how to scale Apache Spark from
O(100) cores up to O(10,000) cores on DOE supercomputers. This had transformative impact on
ML applications in scientific computing. PI Canning at LBNL is a
materials science expert and will lead the collaboration with the
Materials Genome Initiative.    PI Ellingson is an expert in
computational biology and bioinformatics and will lead the effort in
computational drug discovery for cancer treatment. PI de Jong is an
expert in computational chemistry and will lead the effort in mass spectrometry.
 



\newpage

\section*{Conflicts of Interest}


Brad Chamberlain(Cray), D. K. Panda (Ohio State), Vivek Sarkar (Rice), John Mellor Crummey (Rice), Mattan Erez (UT Austin), Ron Brightwell (Sandia),
Jim Laros (Sandia), Ron Oldfield (Sandia), Kevin Pedretti (Sandia),
Mike Lang (LANL), Arthur B. Maccabe (ORNL), David Bernholdt (ORNL),
Christian Engelmann (ORNL), Karsten Schwan (Georgia Tech), Thomas
Sterling (Indiana U), Andrew Lumsdaine (Indiana U), Frank Mueller (NC
State), Peter Dinda (Northwestern), Eric Brewer (UC Berkeley), Patrick
Bridges (UNM), Jack Lange  (U Pittsburg), Koushik Sen (UC Berkeley),
James Demmel (UC Berkeley), Cindy Rubio Gonzales (UC Davis), Xuehai
Qian (USC)

Mark Asta (UC Berkeley), Gregory Bizarri (LBNL), Ramesh Borade (LBNL), Edith Bourret-Courchesne (LBNL), Rostyslav Boutchko (LBNL), Jonathan Carter (LBNL), Hong Ding (UC Berkeley). Jack Dongarra (Univ. Tennessee) Stephen Derenzo (LBNL), Alberto Franchescetti (NREL), Yetta Eagleman (LBNL), Anthony Gamst (UC San Diego), Gautam Gundiah (LBNL), Manisha. Gajbe (Uni. Urbana),  Niels Gronbech-Jensen (UC Davis), Maciej Haranczyk (LBNL), Julien Langou (Univ. of Denver), Steven Louie (UC Berkeley), Osni Marques (LBNL), Bharat Medasani (PNNL), Jeff Neaton (LBNL), Leonid Oliker (LBNL), Kristin Persson (LBNL), John Shalf (LBNL), Stanimire Tomov (Univ. Tennessee),  Marv. Weber (LBNL), Christof Voemel (ETH, Zurich), Lin-Wang Wang (LBNL), Alex Zunger (University of Colorado, Boulder)

%diff by Khaled
David Donofrio (LBNL), George Michelogiannakis (LBNL), Katherine Yelick (LBNL), Bei Wang (Princeton Univ.) Stephane Ethier (Princeton Univ.), Bill Tang (Princeton Univ.), Kamesh Maddury (Penn State),  Torsten Hoefler (ETH, Zurich), Gregory T. Byrd (NCSU),  Eric Rotenberg (NCSU), Andre Seznec (INRIA, France),  Francois Bodin (INRIA, France), Miao Luo (Intel), Jeff Hammond (Intel), Ishfaq Ahmad (UT Arlington), Sanjay Ranka (Univ. of Florida), Joseph Kenny (Sandia), Samuel Knight (Sandia), Jeremiah J. Wilke (Sandia), Joseph P. Kenny (Sandia), 
Anna Krylov (UCSB), Evgeny Epifanovsky (UC Berkeley).
\end{document}
